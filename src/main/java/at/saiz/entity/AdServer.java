package at.saiz.entity;

public class AdServer implements BrauUnionServer {
    private String name, samAccountName, canonicalName, created,
            distinguishedName, dnsHostName, enabled, lastLogonDate,
            modified, operatingSystem, operationSystemServicePack, domain;

    public AdServer(String name, String samAccountName, String canonicalName, String created,
                    String distinguishedName, String dnsHostName, String enabled, String lastLogonDate,
                    String modified, String operatingSystem, String operationSystemServicePack, String domain) {
        this.name = name;
        this.samAccountName = samAccountName;
        this.canonicalName = canonicalName;
        this.created = created;
        this.distinguishedName = distinguishedName;
        this.dnsHostName = dnsHostName;
        this.enabled = enabled;
        this.lastLogonDate = lastLogonDate;
        this.modified = modified;
        this.operatingSystem = operatingSystem;
        this.operationSystemServicePack = operationSystemServicePack;
        this.domain = domain;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSamAccountName() {
        return samAccountName;
    }

    public void setSamAccountName(String samAccountName) {
        this.samAccountName = samAccountName;
    }

    public String getCanonicalName() {
        return canonicalName;
    }

    public void setCanonicalName(String canonicalName) {
        this.canonicalName = canonicalName;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getDistinguishedName() {
        return distinguishedName;
    }

    public void setDistinguishedName(String distinguishedName) {
        this.distinguishedName = distinguishedName;
    }

    public String getDnsHostName() {
        return dnsHostName;
    }

    public void setDnsHostName(String dnsHostName) {
        this.dnsHostName = dnsHostName;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getLastLogonDate() {
        return lastLogonDate;
    }

    public void setLastLogonDate(String lastLogonDate) {
        this.lastLogonDate = lastLogonDate;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    public String getOperationSystemServicePack() {
        return operationSystemServicePack;
    }

    public void setOperationSystemServicePack(String operationSystemServicePack) {
        this.operationSystemServicePack = operationSystemServicePack;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }


}
