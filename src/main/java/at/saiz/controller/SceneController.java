package at.saiz.controller;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.LocalTime;
import java.util.ResourceBundle;

import at.saiz.entity.*;
import at.saiz.io.FileManager;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

public class SceneController implements Initializable {
    @FXML
    private TextArea taStatus;

    private ObservableList<BrauUnionServer> comparedList;

    private ObservableList<BrauUnionServer> serverlistAD;
    private ObservableList<BrauUnionServer> serverlistHP;
    private ObservableList<BrauUnionServer> serverlistBUO;

    private Stage primaryStage;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        primaryStage = null;
        taStatus.setEditable(false);

        // this is for debugging and without pickers
        // also this loads data from resources dir, not local file system
        //fastImport();

        //! place a debugPoint here to inspect all server lists from the fastImport-method.
        System.out.println("finished");
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    /*
        Handle Button - Select AD-exported File (adcomputer.csv)
    */
    public void handleLoadButton1(){
        serverlistAD = FileManager.getInstance().loadListFromCsv(primaryStage);
        if (serverlistAD.size() == 0) {
            addStatus("AD-Serverlist: Import abgebrochen...");
        }else{
            addStatus("Load from " + FileManager.getInstance().getLastLoadedFile());
            addStatus("Anzahl AT1- und BRUN-Server in AD-Liste: " + serverlistAD.size());
        }
    }

    /*
        Handle Button - Select exported File of HP (clientlist.xlsx)
    */
    public void handleLoadButton2(){
        serverlistHP = FileManager.getInstance().loadListFromXLSX(primaryStage);
        if (serverlistHP.size() == 0) {
            addStatus("HP-Serverlist: Import abgebrochen...");
        }else {
            addStatus("Load from " + FileManager.getInstance().getLastLoadedFile());
            addStatus("Anzahl AT1- und BRUN-Server in HP-Liste: " + serverlistHP.size());
        }
    }

    /*
        Handle Button - Select separat generated List of BUO Network Admin (Adolf Kubata - Server.xlsm)
    */
    public void handleLoadButton3(){
        serverlistBUO = FileManager.getInstance().loadListFromXLSM(primaryStage);
        if (serverlistBUO.size() == 0) {
            addStatus("BUÖ-Serverlist: Import abgebrochen...");
        }else {
            addStatus("Load from " + FileManager.getInstance().getLastLoadedFile());
            addStatus("Anzahl Server in BUO-Liste: " + serverlistBUO.size());
        }
    }

    /*
        Comparing the imported lists
    */
    public void compareFiles() throws IOException {
        String dir = chooseDir();
        ObservableList<BrauUnionServer> allServers = null;
        boolean saved = false;
        int countS = 1;

        //Only for debugging
//        System.out.println("START COMPARING \n=========================");
//        System.out.println("AD: " + serverlistAD.size());
//        System.out.println("HP: " + serverlistHP.size());
//        System.out.println("BUO: " + serverlistBUO.size());
//        System.out.println("\n\n=========================");

        if (dir != null && serverlistAD != null && serverlistHP != null && serverlistBUO != null) {
            allServers = FileManager.getInstance().mergeLists(serverlistAD, serverlistHP, serverlistBUO);

            while (!saved) {
                boolean checkExistanceA = new File(dir + "\\ServerEquasion.csv").isFile();
                boolean checkExistanceB = new File(dir + "\\ServerEquasion(" + countS + ").csv").isFile();

                if(!checkExistanceA || !checkExistanceB){
                    if (countS == 1 && !checkExistanceA) {
                        FileManager.getInstance().saveListInCsv(dir + "\\ServerEquasion.csv",
                                allServers, serverlistAD, serverlistHP, serverlistBUO);
                    }else{
                        FileManager.getInstance().saveListInCsv(dir + "\\ServerEquasion(" + countS + ").csv",
                                allServers, serverlistAD, serverlistHP, serverlistBUO);
                    }

                    addStatus("Abgleichung wurde erfolgreich durchgeführt!");
                    addStatus("CSV-File wurde erstellt.");
                    saved = true;
                }
                countS++;
            }
        }
        else{
            if (dir == null) {
                addStatus("Abgleichungsvorgang abgebrochen.");
            }else {
                addStatus("Fehler, bitte alle Files einlesen.");
            }
        }
    }

    /*
    * Add a status-message displayed in the text-area
    */
    private void addStatus(String message){
        taStatus.setText(taStatus.getText()+ " [ " + LocalTime.now() + " ] " + message + "\n");
        taStatus.selectPositionCaret(taStatus.getLength());
        taStatus.deselect();
    }

    /*
    * Set the directory for the exported result list
    */
    private String chooseDir() {
        final DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Speicherort wählen...");

        String home = System.getProperty("user.home");
        directoryChooser.setInitialDirectory(new File(home+"/Downloads/"));

        File dir = directoryChooser.showDialog(primaryStage);
        if (dir != null) {
            addStatus("Speicherort: " + dir.getAbsolutePath() + "\n");
            return dir.getAbsolutePath();
        }else {
            return null;
        }
    }

    /*
    * this is for debugging and without pickers
    * also this loads data from resources dir, not local file system
    * */
    public void fastImport() {
        try {
            serverlistAD = FileManager.getInstance().extractFromFile(new File(getClass().getResource("/data/adcomputer.csv").toURI()));

            serverlistHP = FileManager.getInstance().extractFromFile(new File(getClass().getResource("/data/client_list.xlsx").toURI()));

            serverlistBUO = FileManager.getInstance().extractFromFile(new File(getClass().getResource("/data/Server.xlsm").toURI()));


        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

    }
}
