package at.saiz.entity;

/*
    Interface BrauUnionServer is the Motherclass of ADServer, HPServer and BuoServer
 */
public interface BrauUnionServer {
    String getName();
}
