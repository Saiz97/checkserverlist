package at.saiz.entity;

public class BuoServer implements BrauUnionServer{
    String server, ip, function, vendor,
            system, wsusGroup, sep14, storage,
            alias, dateBought, ageInYears,
            lastTicket, licenseDescription,
            vendor2, msServer, msSql, note;

    public BuoServer(String servername, String ip, String function, String vendor,
                     String system, String wsusGroup, String sep14, String storage,
                     String alias, String dateBought, String ageInYears,
                     String lastTicket, String licenseDescription,
                     String vendor2, String msServer, String msSql, String note) {
        this.server = servername;
        this.ip = ip;
        this.function = function;
        this.vendor = vendor;
        this.system = system;
        this.wsusGroup = wsusGroup;
        this.sep14 = sep14;
        this.storage = storage;
        this.alias = alias;
        this.dateBought = dateBought;
        this.ageInYears = ageInYears;
        this.lastTicket = lastTicket;
        this.licenseDescription = licenseDescription;
        this.vendor2 = vendor2;
        this.msServer = msServer;
        this.msSql = msSql;
        this.note = note;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getWsusGroup() {
        return wsusGroup;
    }

    public void setWsusGroup(String wsusGroup) {
        this.wsusGroup = wsusGroup;
    }

    public String getSep14() {
        return sep14;
    }

    public void setSep14(String sep14) {
        this.sep14 = sep14;
    }

    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getDateBought() {
        return dateBought;
    }

    public void setDateBought(String dateBought) {
        this.dateBought = dateBought;
    }

    public String getAgeInYears() {
        return ageInYears;
    }

    public void setAgeInYears(String ageInYears) {
        this.ageInYears = ageInYears;
    }

    public String getLastTicket() {
        return lastTicket;
    }

    public void setLastTicket(String lastTicket) {
        this.lastTicket = lastTicket;
    }

    public String getLicenseDescription() {
        return licenseDescription;
    }

    public void setLicenseDescription(String licenseDescription) {
        this.licenseDescription = licenseDescription;
    }

    public String getVendor2() {
        return vendor2;
    }

    public void setVendor2(String vendor2) {
        this.vendor2 = vendor2;
    }

    public String getMsServer() {
        return msServer;
    }

    public void setMsServer(String msServer) {
        this.msServer = msServer;
    }

    public String getMsSql() {
        return msSql;
    }

    public void setMsSql(String msSql) {
        this.msSql = msSql;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String getName() {
        return getServer();
    }
}
