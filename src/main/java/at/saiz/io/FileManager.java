package at.saiz.io;

import at.saiz.entity.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

public class FileManager {
    private static FileManager instance;
    private FileChooser fileChooser;
    private String lastLoadedFile;

    private FileManager(){
        fileChooser = new FileChooser();

        try {
            fileChooser.setInitialDirectory(new File(".").getCanonicalFile());
        } catch (IOException e) {
            e.printStackTrace();
        }

        lastLoadedFile = "";
    }

    /*
        FileManager is a singleton-class
        Function getInstance is providing access to this class
     */
    public static FileManager getInstance() {
        if(instance == null){
            instance = new FileManager();
        }
        return instance;
    }

    /*
    * opens the FileChooser, import is limited on the files typ/ending.
    */
    public void openFileChooser(String input) {
        switch (input){
            case "csv":
                fileChooser = new FileChooser();
                fileChooser.setTitle("Choose CSV-File");
                try {
                    fileChooser.setInitialDirectory(new File(".").getCanonicalFile());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Comma Separated Values File (*.csv)", "*.csv"));

                break;

            case "xlsx":
                fileChooser = new FileChooser();
                fileChooser.setTitle("Choose XLSX-File");
                try {
                    fileChooser.setInitialDirectory(new File(".").getCanonicalFile());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Excel Spreadsheet (*.xlsx)", "*.xlsx"));
                break;

            case "xlsm":
                fileChooser = new FileChooser();
                fileChooser.setTitle("Choose XLSM-File");
                try {
                    fileChooser.setInitialDirectory(new File(".").getCanonicalFile());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Excel Spreadsheet with Makros (*.xlsm)", "*.xlsm"));
                break;
        }
    }

    /*
        Read - Comma Separated Values File
        Save essential elements in List
    */
    public ObservableList loadListFromCsv(Stage stage){
        openFileChooser("csv");
        File file = fileChooser.showOpenDialog(stage);
        if(file != null) return extractFromFile(file);
        return FXCollections.observableArrayList();
    }

    /*
        Read - Excel Spreadsheet without Makros
        Save essential elements in List
    */
    public ObservableList loadListFromXLSX(Stage stage){
        openFileChooser("xlsx");
        File file = fileChooser.showOpenDialog(stage);
        if(file != null) return extractFromFile(file);
        return FXCollections.observableArrayList();
    }

    /*
        Read - Excel Spreadsheet with Makros
        Save essential elements in List
    */
    public ObservableList loadListFromXLSM(Stage stage){
        openFileChooser("xlsm");
        File file = fileChooser.showOpenDialog(stage);
        if(file != null) return extractFromFile(file);
        return FXCollections.observableArrayList();
    }

    /*
        Take data of files into lists
    */
    public ObservableList extractFromFile(File file) {
        lastLoadedFile = file.getPath();

        if(file.getName().endsWith("csv")) {
            // do csv stuff
            if (file != null) {
                try {
                    List<String> lines = Files.readAllLines(file.toPath());
                    ObservableList<AdServer> adServers = FXCollections.observableArrayList();
                    String currentLine = null;

                    for (int i = 0; i < lines.size(); i++) {

                        if(currentLine != null)
                            currentLine = currentLine + "\n" + lines.get(i);

                        if(currentLine == null)
                            currentLine = lines.get(i);

                        String[] data = currentLine.split(";");
                        String[] data_ = new String[12];
                        //System.out.println(data[0]);
                        try {
                            if (data[0].toString().substring(1, 5).toUpperCase().equals("AT1S")
                                    || data[0].toString().toUpperCase().substring(1, 6).equals("BRUNS")) {
                                for (int j = 0; j < data.length; j++) {
                                    if (data[j].length() != 0) {
                                        //removes Quotationmarks
                                        data_[j] = data[j].toString().substring(1, data[j].toString().length() - 1);
                                    }
                                }

                                adServers.add(new AdServer(
                                        data_[0], data_[1], data_[2],
                                        data_[3], data_[4], data_[5],
                                        data_[6], data_[7], data_[8],
                                        data_[9], data_[10], data_[11]));
                            }
                            currentLine = null;
                        } catch (Exception ignored) {

                        }
                    }
                    //Collections.sort(servers);
                    return adServers;

                } catch (IOException e) {
                    e.printStackTrace();
                    return FXCollections.observableArrayList();
                }
            }
        } else if(file.getName().endsWith("xlsx")) {
            // do xlsx stuff
            List<HpServer> data = new LinkedList<>();

            try(InputStream execlFileToRead = new FileInputStream(file)) {
                XSSFWorkbook workbook = new XSSFWorkbook(execlFileToRead);
                XSSFSheet sheet = workbook.getSheetAt(0);

                XSSFRow row;

                Iterator<Row> rows = sheet.rowIterator();

                while (rows.hasNext()) {
                    row = (XSSFRow) rows.next();
                    XSSFCell cell = row.getCell(0);
                    if (cell != null) {
                        if (cell.getCellTypeEnum() == CellType.STRING) {
                            //Check if data is valid for usage - Filter AT1-Server & BRUN-Server
                            if (cell.getStringCellValue().split("\\.")[0].toString().substring(0, 4).toUpperCase().equals("AT1S")
                                    || cell.getStringCellValue().split("\\.")[0].toString().substring(0, 5).toUpperCase().equals("BRUNS")) {
                                data.add(
                                    /*
                                    * separates the String of the first column
                                    * gets servername (split[0]), domainname (split[2])
                                    */
                                        new HpServer(
                                                cell.getStringCellValue().split("\\.")[0],
                                                cell.getStringCellValue().split("\\.")[2])
                                );
                            }
                        }
                    }
                }
                return FXCollections.observableList(data);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if(file.getName().endsWith("xlsm")) {
            //do xlsm stuff
            List<BuoServer> data = new LinkedList<>();

            try(InputStream execlFileToRead = new FileInputStream(file)) {
                XSSFWorkbook workbook = new XSSFWorkbook(execlFileToRead);
                XSSFSheet sheet = workbook.getSheetAt(0);
                DataFormatter formatter = new DataFormatter(); //creating formatter using the default locale
                CellRangeAddress range = null;

                for (int i=1; i <= sheet.getLastRowNum(); i++){
                    if (sheet.getRow(i) == null){
                        //System.out.println("");
                        continue;
                    }
                    else if (Objects.equals(formatter.formatCellValue(sheet.getRow(i).getCell(1)), "")
                            || formatter.formatCellValue(sheet.getRow(i).getCell(1)).startsWith("Enclosure")) {
                        continue;
                    } else if (getNbOfMergedRegions(sheet, i) > 2) {
                        //System.out.println("row [" + i + "] -> " + getNbOfMergedRegions(sheet, i) + " merged regions");
                        continue;
                    }

                    //Returns the formatted value of a cell as a String regardless of the cell type.
                    String servername = formatter.formatCellValue(sheet.getRow(i).getCell(1));
                    String ip = formatter.formatCellValue(sheet.getRow(i).getCell(2));
                    String function = formatter.formatCellValue(sheet.getRow(i).getCell(3));
                    String vendor = formatter.formatCellValue(sheet.getRow(i).getCell(4));
                    String system = formatter.formatCellValue(sheet.getRow(i).getCell(5));
                    String wsusGroup = formatter.formatCellValue(sheet.getRow(i).getCell(6));
                    String sep14 = formatter.formatCellValue(sheet.getRow(i).getCell(7));
                    String storage = formatter.formatCellValue(sheet.getRow(i).getCell(8));
                    String alias = formatter.formatCellValue(sheet.getRow(i).getCell(9));
                    String dateBought = formatter.formatCellValue(sheet.getRow(i).getCell(10));
                    String ageInYears = formatter.formatCellValue(sheet.getRow(i).getCell(11));
                    String lastTicket = formatter.formatCellValue(sheet.getRow(i).getCell(12));
                    String licenseDescription = formatter.formatCellValue(sheet.getRow(i).getCell(14));
                    String vendor2 = formatter.formatCellValue(sheet.getRow(i).getCell(15));
                    String msServer = formatter.formatCellValue(sheet.getRow(i).getCell(16));
                    String msSql = formatter.formatCellValue(sheet.getRow(i).getCell(17));
                    String note = formatter.formatCellValue(sheet.getRow(i).getCell(18));

                    if (servername != null && servername != "") {
//                        if (servername.toUpperCase().startsWith("AT1S") || servername.toUpperCase().startsWith("BRUNS"))
                        data.add(
                                new BuoServer(servername, ip,
                                        function, vendor,
                                        system, wsusGroup,
                                        sep14, storage,
                                        alias, dateBought,
                                        ageInYears, lastTicket,
                                        licenseDescription, vendor2,
                                        msServer, msSql, note)
                        );
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return FXCollections.observableList(data);
        }
        return null;
    }

    //handle merged cells
    //https://poi.apache.org/apidocs/org/apache/poi/ss/util/CellRangeAddress.html
    //https://stackoverflow.com/questions/15755493/how-to-determine-merged-cells-in-a-certain-row
    private static int getNbOfMergedRegions(Sheet sheet, int row)
    {
        int count = 0;

        for(int i = 1; i < sheet.getNumMergedRegions(); ++i)
        {
            CellRangeAddress range = sheet.getMergedRegion(i);
            if (range.getFirstRow() <= row && range.getLastRow() >= row)
                ++count;
        }
        return count;
    }

    /*
       * join all 3 lists together, and filter doubles
       * result list is a list of BrauUnionServer, mother of all 3 Servertypes
       * */
    public ObservableList<BrauUnionServer> mergeLists (ObservableList<BrauUnionServer> ad, ObservableList<BrauUnionServer> hp, ObservableList<BrauUnionServer> buo){
        ObservableList<BrauUnionServer> combindedList = FXCollections.observableArrayList();
        ObservableList<BrauUnionServer> result = FXCollections.observableArrayList();

        combindedList.addAll(ad);
        combindedList.addAll(hp);
        combindedList.addAll(buo);

        Map<String, List<BrauUnionServer>> mergedCollection = combindedList.stream().collect(
                Collectors.groupingBy(server -> server.getName().toUpperCase(), Collectors.toList())
        );
        
        //without doubles in comparedServers
        mergedCollection.values().forEach(value -> result.add(value.get(0)));

        Collections.sort(result,
                new Comparator<BrauUnionServer>()
                {
                    public int compare(BrauUnionServer s1, BrauUnionServer s2)
                    {
                        return s1.getName().toUpperCase().compareTo(s2.getName().toUpperCase());
                    }
                });

        return result;
    }

    /*
    * Write data of the result list into a file
    * https://examples.javacodegeeks.com/core-java/writeread-csv-files-in-java-example/
     */
    public void saveListInCsv(String csvName,
                              ObservableList<BrauUnionServer> allServers,
                              ObservableList<BrauUnionServer> ad,
                              ObservableList<BrauUnionServer> hp,
                              ObservableList<BrauUnionServer> buo) throws IOException {
        //Delimiter used in CSV file
        String COMMA_DELIMITER = ";";
        String NEW_LINE_SEPARATOR = "\n";

        //CSV file header
        String FILE_HEADER = "Servername;ActiveDirectory;HP-Dataprotector;BUÖ-Serverliste";

        FileWriter fileWriter = null;
        Boolean check = false;

        try {
            fileWriter = new FileWriter(csvName);

            //Write the CSV file header
            fileWriter.append(FILE_HEADER.toString());

            //Add a new line separator after the header
            fileWriter.append(NEW_LINE_SEPARATOR);

            //Write a new student object list to the CSV file
            for (BrauUnionServer server : allServers) {
                fileWriter.append(String.valueOf(server.getName()));
                fileWriter.append(COMMA_DELIMITER);

                for (BrauUnionServer sAD : ad) {
                    check = false;
                    if (Objects.equals(server.getName().toUpperCase(), sAD.getName().toUpperCase())) {
                        fileWriter.append("Ja");
                        fileWriter.append(COMMA_DELIMITER);
                        check = true;
                        break;
                    }
                }

                if (!check) fileWriter.append(COMMA_DELIMITER);

                for (BrauUnionServer sHP : hp) {
                    check = false;
                    if (Objects.equals(server.getName().toUpperCase(), sHP.getName().toUpperCase())) {
                        fileWriter.append("Ja");
                        fileWriter.append(COMMA_DELIMITER);
                        check = true;
                        break;
                    }
                }

                if (!check) fileWriter.append(COMMA_DELIMITER);

                for (BrauUnionServer sBUO : buo) {
                    check = false;
                    if (Objects.equals(server.getName().toUpperCase(), sBUO.getName().toUpperCase())) {
                        fileWriter.append("Ja");
                        fileWriter.append(COMMA_DELIMITER);
                        check = true;
                        break;
                    }
                }
                if (!check) fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(NEW_LINE_SEPARATOR);
            }

            System.out.println("CSV file was created successfully!");

        } catch (Exception e) {
            System.out.println("Error in Csv-FileWriter!");
            e.printStackTrace();
        } finally {
            try {
                assert fileWriter != null;
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter!");
                e.printStackTrace();
            }
        }


//        for (int i = 0; i < compList.size(); i++) {
//        //System.out.println(compList.get(i).getName());
//        writer.write(compList.get(i).getName() + "\n");}
    }

    public String getLastLoadedFile(){
        return lastLoadedFile;
    }
}
